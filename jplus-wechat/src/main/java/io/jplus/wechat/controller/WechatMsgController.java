/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.wechat.controller;

import com.jfinal.log.Log;
import com.jfinal.weixin.sdk.jfinal.MsgControllerAdapter;
import com.jfinal.weixin.sdk.msg.in.InTextMsg;
import com.jfinal.weixin.sdk.msg.in.event.InFollowEvent;
import com.jfinal.weixin.sdk.msg.in.event.InMenuEvent;
import io.jboot.web.controller.annotation.RequestMapping;

/**
 * @author Retire 吴益峰 （372310383@qq.com）
 * @version V1.0
 * @Package io.jplus.wechat
 */
@RequestMapping("/msg")
public class WechatMsgController extends MsgControllerAdapter {

    private static final Log log = Log.getLog(WechatMsgController.class);

    @Override
    protected void processInFollowEvent(InFollowEvent inFollowEvent) {

        // 关注与取消关注时调用，取消关注时无法收到消息
        if (InFollowEvent.EVENT_INFOLLOW_SUBSCRIBE.equals(inFollowEvent
                .getEvent())) {
            //发送关注消息
            renderOutTextMsg("欢迎关注微信公众号！");
        }
    }

    @Override
    protected void processInTextMsg(InTextMsg inTextMsg) {

    }

    @Override
    protected void processInMenuEvent(InMenuEvent inMenuEvent) {

    }
}
