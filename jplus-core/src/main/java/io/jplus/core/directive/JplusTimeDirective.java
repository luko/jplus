/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.core.directive;

import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;

@JFinalDirective("JplusTime")
public class JplusTimeDirective extends JbootDirectiveBase {
    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        String id = getParam("id", scope);
        String name = getParam("name", scope);
        boolean time = getParam("isTime", false, scope);
        String format = getParam("format", "YYYY-MM-DD", scope);
        String html = "<div class=\"input-group\">\n" +
                "    <div class=\"input-group-btn\">\n" +
                "        <button data-toggle=\"dropdown\" class=\"btn btn-white dropdown-toggle\"\n" +
                "                type=\"button\">" + name + "</button>\n" +
                "    </div>\n" +
                "    <input type=\"text\" class=\"form-control layer-date\"\n" +
                "           onclick=\"laydate({istime: " + time + ", format: '" + format + "'})\" id=\"" + id + "\"/>\n" +
                "</div>";
        write(writer, html);


    }
}
