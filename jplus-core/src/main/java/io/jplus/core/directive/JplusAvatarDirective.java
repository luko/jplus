/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 *
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.core.directive;

import com.jfinal.kit.StrKit;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;


@JFinalDirective("JplusAvatar")
public class JplusAvatarDirective extends JbootDirectiveBase {
    @Override
    public void onRender(Env env, Scope scope, Writer writer) {

        String id = getParam("id", scope);
        String name = getParam("name", scope);
        String avatarImg = getParam("avatarImg", "", scope);
        String imageUrl = "${CPATH}/static/img/girl.gif";
        if (StrKit.notBlank(avatarImg)) {
            imageUrl = "${CPATH}/static/img/" + avatarImg;
        }
        boolean underline = getParam("underline", false, scope);
        StringBuffer html = new StringBuffer();
        html.append("<div class=\"form-group\">\n" +
                "    <label class=\"col-sm-3 control-label head-scu-label\">" + name + "</label>\n" +
                "    <div class=\"col-sm-4\">\n" +
                "        <div id=\"" + id + "PreId\">\n" +
                "            <div><img width=\"100px\" height=\"100px\" src=\"" + imageUrl + "\"></div></div>\n" +
                "    </div>\n" +
                "    <div class=\"col-sm-2\">\n" +
                "        <div class=\"head-scu-btn upload-btn\" id=\"" + id + "BtnId\">\n" +
                "            <i class=\"fa fa-upload\"></i>&nbsp;上传\n" +
                "        </div>\n" +
                "    </div>\n" +
                "    <input type=\"hidden\" id=\"" + id + "\" value=\"" + avatarImg + "\"/>\n" +
                "</div>");

        if (underline) {
            html.append("<div class=\"hr-line-dashed\"></div>");
        }
        write(writer, html.toString());
    }
}
